# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [0.1.4] - 2020-06-26
### Changed
- Depends on **mva-core 0.4.0** or greater (bus monitor improvements)

## [0.1.3] - 2020-06-22
### Changed
- Depends on **mva-core 0.3.0** or greater (Observe API updates, minor logging updates)

## [0.1.2] - 2020-05-02
### Changed
- Depends on **mva-core 0.2.2** or greater (empty string mediator and API improvements)
### Removed
- Previously deprecated ***Publish ViewModel Data.vi***.
- Unclaimed and non-functioning actor auto-registration code (old stuff)

## [0.1.1] - 2020-04-03
### Fixed
- Wrong conditional disable flags (and conditionally broken code) in ViewModel. (See Readme for valid conditional flags.)

## [0.1.0] - 2020-01-22
### Added
- Initial release of mva viewable components as stand-alone package
### Removed
- IDialogBox removed from this release