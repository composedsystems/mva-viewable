# MVA Viewable

All the user interface pieces to assemble and run a complete (with UI) MVA separation of concerns application on a PC. This package is a dependency of the complete MVA Framework, which extends **mva-viewable** by adding more bells and whistles.

## Important Semantics

*Come back and reread this section after getting familiar with the framework.*

- Viewable hierarchies (Viewables nested in ViewManagers) launch automatically at startup if properly assembled before launch using framework constructors.
- There is no limit to the depth of Viewable hierarchies (ViewManagers can contain any depth of nested ViewManagers and Viewables)
- By default, events aggregated in Viewables and ViewManagers propagate up to the ViewModel (root).

## Conditional Symbols

Application framework logging tracks the launching, stopping and collection of framework actors. The following conditional symbols can add or remove logging to suit your need and preference:

- **Default** (no project-defined symbols) - Log basic events like launch, stop, error, and collection.
- **ENABLE_VERBOSE_LOG** - Log all of the Default stuff, plus extra mediation activity.
- **REMOVE_FRAMEWORK_LOG** - Log nothing for enhanced performance or because you just like that fresh commando feeling.

## Included Components

### EventAggregator
An ancestor of IViewable that lets Viewable things register for and handle events in ***Receive [event name] Event*** overrides in Actor Core right out of the box (no need to make any actor messages). Just register for the desired event in actor's Actor Core override and then override the *Receive* method.

### IViewable
An actor that can be dynamically insterted into a ProtectedSubpanel or shown to provide a user interface. IViewable descends from EventAggregator.

#### ProtectedSubpanel:

- Protects subpanels from insertion/removal race conditions.
- Returns callback message to removed view for optional 'not shown' cleanup.

### IViewManager
A viewable actor that arranges (i.e. manages) one or more other views. ViewManagers have one or more protected subpanels through which they manage their views. IViewManager descends from IViewable.

### IViewModel
An application root actor responsible for managing top-level viewable state. ViewModel launches mediator, model hierarchy, and viewable hierarchy. IViewModel descends from IViewManager and IViewable.